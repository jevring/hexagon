package v2;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

/**
 * @author markus.jevring@petercam.corp
 *         Created: 2011-01-16
 */
public class GameEngine extends MouseAdapter {
    private final List<List<Hexagon>> hexagons;
    private final JPanel canvas;

    public GameEngine(List<List<Hexagon>> hexagons, JPanel canvas) {
        this.hexagons = hexagons;
        this.canvas = canvas;
    }

    @Override
    public void mousePressed(MouseEvent e) {
        Hexagon otherSelectedShape = null;
        Point previouslySelectedHexagonLocation = null;

        for (int x = 0; x < hexagons.size(); x++) {
            List<Hexagon> column = hexagons.get(x);
            for (int y = 0; y < column.size(); y++) {
                Hexagon hexagon = column.get(y);
                Polygon polygon = hexagon.getShape();
                if (polygon.contains(e.getPoint())) {
                    hexagon.setSelected(!hexagon.isSelected());
                }
                if (hexagon.isSelected()) {
                    if (otherSelectedShape == null) {
                        otherSelectedShape = hexagon;
                        previouslySelectedHexagonLocation = new Point(x, y);
                    } else {
                        if (isNeighbor(previouslySelectedHexagonLocation, new Point(x, y))) {
                            Color c = otherSelectedShape.getColor();
                            otherSelectedShape.setColor(hexagon.getColor());
                            hexagon.setColor(c);
                        } else {
                            // todo: can't select this, do some indication
                            System.out.println("The two hexagons are not neighbors");
                        }
                        hexagon.setSelected(false);
                        otherSelectedShape.setSelected(false);
                    }
                }
            }
        }
        /*
        For each neighbor:
            if the neighbor is "the same color", check its neighbor in the same direction
            if we have more than 3 neighbors, the hexagons are recycled (currently just mark them black)


        Neighbors will obviously be the one just before and after in the column.
        Neighbors will also be the ones just before and after in the ADJOINING columns.

        neighbors(h[i][j]) = {h[i][j-1], h[i][j+1], h[i-1][j-1], h[i-1][j+1], h[i+1][j-1], h[i+1][j+1]}

         */

        canvas.repaint();

        /*

        Game idea:
        Similarly to bejeweled, we can make combinations. Down, and diagonally up and diagonally down.
        When a match is made, two new colors blend into being in their place.

         */

    }

    private boolean isNeighbor(Point a, Point b) {
        boolean sameColumn = a.x == b.x;
        boolean adjacentColumn = a.x == b.x + 1 || a.x == b.x - 1;
        boolean directlyAboveOrBelowInAdjacentColumn = a.y == b.y || a.y == b.y - 1 || b.y == a.y - 1;
        boolean directlyAboveOrBelowInSameColumn = a.y == b.y + 1 || a.y == b.y - 1;

        System.out.println();
        System.out.println("a = " + a);
        System.out.println("b = " + b);
        System.out.println("sameColumn = " + sameColumn);
        System.out.println("adjacentColumn = " + adjacentColumn);
        System.out.println("directlyAboveOrBelowInAdjacentColumn = " + directlyAboveOrBelowInAdjacentColumn);
        System.out.println("directlyAboveOrBelowInSameColumn = " + directlyAboveOrBelowInSameColumn);
        System.out.println("neighbors = " + ((sameColumn && directlyAboveOrBelowInSameColumn) || (adjacentColumn && directlyAboveOrBelowInAdjacentColumn)));

        return (sameColumn && directlyAboveOrBelowInSameColumn) || (adjacentColumn && directlyAboveOrBelowInAdjacentColumn);

/*
        if (a.x == b.x) { // same column
            return a.y == b.y + 1 || a.y == b.y - 1; // above or below
        } else if (a.x == b.x + 1 || a.x == b.x - 1) { // adjacent columns
            return a.y == b.y + 1 || a.y == b.y - 1; // above or below
        } else {
            // more than one column apart
            return false;
        }
*/
    }
}
