package hexagons.graphics;

import java.awt.*;

/**
 * @author markus@jevring.net
 *         Date: 2011-01-13-18:52
 */
public class Hexagon {
    public static final int ROTATION_FLAT = 30;
    public static final int ROTATION_STANDING = 0;

    private int radius;
    private int rotation = ROTATION_FLAT;
    private Point location;
    private boolean selected = false;
    private Polygon shape = null;

    private float rate = (float) Math.random() * 0.1f;
    private float hue = (float) Math.random();
    private float saturation = 0.5f;
    private float brightness = 1.0f; // WAS 0.5
    private Color color = Color.getHSBColor(hue, saturation, brightness);
    private boolean used = false;
    private long usedTime;


    public Hexagon(int radius) {
        this.radius = radius;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public void setLocation(Point location) {
        this.location = location;
    }

    public Point getLocation() {
        return location;
    }

    public Polygon getShape() {
        if (shape == null) {
            shape = hexagon(location, radius, rotation);
        }
        return shape;
    }

    public boolean isUsed() {
        return used;
    }

    public void setUsed(boolean used) {
        this.used = used;
        if (used) {
            usedTime = System.currentTimeMillis();
        } else {
            usedTime = 0;
        }
    }

    public long getUsedTime() {
        return usedTime;
    }

    public float getHue() {
        return hue;
    }

    public void setHue(float hue) {
        this.hue = hue;
    }

    public float getRate() {
        return rate;
    }

    public float getSaturation() {
        return saturation;
    }

    public void setSaturation(float saturation) {
        this.saturation = saturation;
    }

    public float getBrightness() {
        return brightness;
    }

    public void setBrightness(float brightness) {
        this.brightness = brightness;
    }

    /**
     *
     * @param p the center point of the hexagon.
     * @param radius the radius to the points, since it is the only one that is fixed.
     * @param rotation the rotation, in degrees, 0-360.
     * @return a hexagon.
     */
    private Polygon hexagon(Point p, int radius, int rotation) {
        int[] xs = new int[7];
        int[] ys = new int[7];

        /*
          the first point is $rotation degrees from the starting point, $radius units out.
          the next point is 60 degrees clockwise from that, $radius units out
         */

        for (int i = 0; i <= 6; i++) {
            xs[i] = (int) (p.x + Math.sin(Math.toRadians(rotation + (60 * i))) * radius);
            ys[i] = (int) (p.y + Math.cos(Math.toRadians(rotation + (60 * i))) * radius);
        }

        // close the polygon
        xs[6] = xs[0];
        ys[6] = ys[0];

        return new Polygon(xs, ys, 7);
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }
}

