package v1;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * @author markus@jevring.net
 *         Date: 2011-01-13-18:52
 */
public class Main {
    private static int rotation = 0;
    private static int radius = 50;
    private static final int STANDING = 0;
    private static final int FLAT = 30;
    static final Color[] colors = new Color[]{Color.RED, Color.GREEN, Color.BLUE, Color.YELLOW, Color.MAGENTA, Color.CYAN, Color.ORANGE};
    private static Point mouse = new Point(0,0);
    private static int gridWidth = 4;
    private static int gridHeight = 12;
    //private static Hexagon[][] hexagons = new Hexagon[gridWidth][gridHeight];
    private static List<List<Hexagon>> hexagons;

    public static void main(String[] args) {
        rotation = FLAT;
/*
        // create hexagons
        for (int i = 0; i < gridWidth; i++) {
            for (int j = 0; j < gridHeight; j++) {
                hexagons[i][j] = new Hexagon();
            }
        }
*/




        final JFrame frame = new JFrame("Hexagon");
        final JPanel canvas = new JPanel() {
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g); // paints the background
                //grid(g, frame.getHeight(), frame.getWidth());
                //tesselation(g, frame.getHeight(), frame.getWidth());
                //offsetGridTesselation(g, frame.getHeight(), frame.getWidth());
                //fixedSizeGridTesselation(g, frame.getHeight(), frame.getWidth());
                drawHexagons(g);
            }
        };
        canvas.setBackground(Color.BLACK);
        canvas.addMouseMotionListener(new MouseMotionAdapter() {
            public void mouseMoved(MouseEvent e) {
                mouse = e.getPoint();
                canvas.repaint();
            }
        });
        canvas.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                List<Float> selectedHues = new ArrayList<Float>();
                for (List<Hexagon> column : hexagons) {
                    for (Hexagon hexagon : column) {
                        Polygon polygon = hexagon.draw();
                        if (polygon.contains(e.getPoint())) {
                            hexagon.setSelected(!hexagon.isSelected());
                        }
                        if (hexagon.isSelected()) {
                            selectedHues.add(hexagon.getHue()); // this assumes we can calculate fast enough so that this doesn't become an issue.
                        }
                        // todo: try to get the neighbors here and color them as well, just to see if it is easy to discern

                        /*
                        Neighbors will obviously be the one just before and after in the column.
                        Neighbors will also be the ones just before and after in the ADJOINING columns.

                        neighbors(h[i][j]) = {h[i][j-1], h[i][j+1], h[i-1][j-1], h[i-1][j+1], h[i+1][j-1], h[i+1][j+1]}
                         */
                    }
                }
                canvas.repaint();
                if (selectedHues.size() > 1) {
                    // if the hues of the selected hexagons are withing $limit of each other, the player gets points.
                    float maxHueDifference = 0.0f;
                    float lastHue = -1.0f;
                    for (Float selectedHue : selectedHues) {
                        //System.out.println("selectedHue = " + selectedHue);
                        if (lastHue != -1.0f) {
                            maxHueDifference = Math.max(Math.abs(selectedHue - lastHue), maxHueDifference);
                        }
                        lastHue = selectedHue;
                    }
                    System.out.println("maxHueDifference = " + maxHueDifference);
                    if (maxHueDifference < 0.2f) {
                        System.out.println("+1 point");
                    }
                }

                /*

                Game idea:
                Similarly to bejeweled, we can make combinations. Down, and diagonally up and diagonally down.
                When a match is made, two new colors blend into being in their place.

                 */

            }
        });
        JPanel p = new JPanel(new BorderLayout());
        p.add(canvas, BorderLayout.CENTER);
        final JSlider slider = new JSlider(0, 100);
        slider.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                radius = slider.getValue();
                canvas.revalidate();
                canvas.repaint();
            }
        });
        p.add(slider, BorderLayout.SOUTH);
        // control the size of the board, not the size of the window.
        final Dimension size = new Dimension(800, 480);
        canvas.setMinimumSize(size);
        canvas.setPreferredSize(size);
        frame.add(canvas);
        frame.pack();
        frame.setLocation(600, 600);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        createHexagons(frame.getHeight(), frame.getWidth());

        ColorChanger cc = new ColorChanger(hexagons, 0.15f, canvas);
        cc.start(); // this must call repaint() when all colors are updated

        frame.setVisible(true);
    }

    private static void drawHexagons(Graphics g) {
        Graphics2D g2d = (Graphics2D)g;
        g2d.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_DEFAULT);
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON); // WORLD OF DIFFERENCE!
        for (List<Hexagon> column : hexagons) {
            for (Hexagon hexagon : column) {
                Polygon polygon = hexagon.draw();
                g.setColor(hexagon.getColor());
                g.fillPolygon(polygon);

                g.setColor(Color.LIGHT_GRAY);
                // the game tiles should probably always have some form of border. It's just that the border changes when it is highlighted

                if (polygon.contains(mouse) || hexagon.isSelected()) { // todo: obviously when we're doing this for android, we can't use the mouse-over
                    g.setColor(Color.WHITE);
                    g2d.setStroke(new BasicStroke(5f));
                } else {
                    g2d.setStroke(new BasicStroke(2f));
                }
                g.drawPolygon(polygon);
            }
        }
    }

    private static void createHexagons(int height, int width) {
        /*
        drawing hexagons in a grid is easy. Each one is just one full distance down
        or half a distance down and half a distance out
         */
        hexagons = new ArrayList<List<Hexagon>>(); // recreate the set when we resize the panel

        boolean offset = true;
        int border = 10;
        int flatRadius = (int) (radius * Math.cos(Math.toRadians(30)));
        int offsetDistance = (int) (radius * Math.sin(Math.toRadians(30))) + radius;
        for (int x = radius; x < width; x += offsetDistance + border) {
            // todo: change this so that there is a "frame" or partial hexagons framing the entire game board.
            // this frame should also pulsate, but should not be selectable.
            // only fully visible hexagons should be selectable.


            // _todo: every other line needs to be offset by radius
            List<Hexagon> column = new ArrayList<Hexagon>();
            hexagons.add(column);
            //for (int y = (flatRadius * (offset ? 1 : 2)) + (offset ? 0 : border / 2); y < height; y += (flatRadius * 2) + border) {
            int startingPoint;
            if (offset) {
                // ((flatRadius * (offset ? 1 : 2)) + (offset ? 0 : border / 2))
                startingPoint = flatRadius;
            } else {
                //startingPoint = -((flatRadius * 2) + border + (border / 2)) ;
                startingPoint = -(flatRadius / 2) + border + (border / 2) ;
            }
            for (int y = startingPoint; y < height; y += (flatRadius * 2) + border) {
                Hexagon h = new Hexagon(radius);
                h.setLocation(new Point(x, y));
                column.add(h);
            }
            offset = !offset;
        }
    }

    private static void fixedSizeGridTesselation(Graphics g, int height, int width) {
        for (int i = 0; i < gridWidth; i++) {
            for (int j = 0; j < gridHeight; j++) {
                //hexagons[i][j] = new Hexagon();
                // todo: calculate size and location for the hexagon
                // todo: we probably shouldn't do this on repaint(), only on revalidate() (i.e. when the size changes)



                // actually, we're probably better off setting a fixed size on the hexagons.
                // that way the size of the game plan doesn't actually matter, and we can grow comfortably
                // to other sizes and shapes.
            }
        }
    }

    private static void offsetGridTesselation(Graphics g, int height, int width) {
        /*
        drawing hexagons in a grid is easy. Each one is just one full distance down
        or half a distance down and half a distance out
         */
        // todo: ideally we should define the grid of hexagons and then scale their size based on the size of the window.
        // not scale the number of hexagons.
        int hexagons = 0;
        boolean offset = true;
        int border = 10;
        int flatRadius = (int) (radius * Math.cos(Math.toRadians(30)));
        int offsetDistance = (int) (radius * Math.sin(Math.toRadians(30))) + radius;
        // todo: if we start at -radius and -flatRadius, respectively, we can fill the whole thing
        for (int x = radius; x < width; x += offsetDistance + border) {
            // _todo: every other line needs to be offset by radius
            for (int y = (flatRadius * (offset ? 1 : 2)) + (offset ? 0 : border / 2); y < height; y += (flatRadius * 2) + border) {

                // todo: change this so that we have a set array of hexagons, and that they change size based on the window, rather than the amount of hexagons changing.


                Hexagon h = new Hexagon(radius);
                h.setLocation(new Point(x, y));
                Polygon polygon = h.draw();
                g.setColor(h.getColor());
                if (polygon.contains(mouse)) {
                    g.fillPolygon(polygon);
                } else {
                    g.drawPolygon(polygon);
                }
            }
            offset = !offset;
        }
    }

    private static void tesselation(Graphics g, int height, int width) {
        Point center = new Point(width / 2, height / 2);
        Point p = center;
        Color c = Color.RED;
        int hexagons = 0;
        /*
        to make this in to something, we can either draw from the very center and keep expanding our radius,
        or we can do it in a logical way where we say "unless already present, draw a hexagon that is attached
        to each of this hexagon's sides".

        The latter I think is better, because then we can reason about it. it's not just graphics generation.

        Do it for just one to start with
         */
        List<Hexagon> hs = new LinkedList<Hexagon>();
        Hexagon h = new Hexagon(radius);
        h.setLocation(center);
        hs.add(h);
        g.fillPolygon(draw(h, null, -1));
        do {
            for (int i = 0; i < 6; i++) {
                if (!h.hasNeighbor(i)) {
                    Hexagon n = new Hexagon(radius);
                    h.setNeighbor(n, i);
                    // todo: create and set neighbor
                    // todo: do a breadth first search. that will make things round.
                    // a depth first search will make a line
                    g.setColor(colors[++hexagons % 5]);
                    g.fillPolygon(draw(n, h, i));
                    hs.add(n);
                }
            }
            h = hs.get(hexagons);
        } while (hs.size() < 5);
/*
        do {
            c = colors[hexagons++ % 6];
            g.setColor(c);
            Hexagon h = new Hexagon(radius);
            g.fillPolygon(h.getShape(p));
            System.out.println("Drawing hexagon at " + p);
            int flatRadius = (int) (radius * Math.cos(Math.toRadians(30)));
            int distance = 2 * flatRadius;
            int newX = (int) (distance * Math.cos(Math.toRadians(30)));
            int newY = (int) (distance * Math.sin(Math.toRadians(30)));
            p = new Point(p.x + newX, p.y + newY);
            // todo: this gets them in a diagonal line. how do we get them in a circle pattern from the first?
            // or any other pattern that fills the screen?
        } while (hexagons < 20);
        //} while (p.x < width && p.y < height);
*/
    }

    private static Polygon draw(Hexagon neighbor, Hexagon h, int neighborIndex) {
        if (h != null) { // avoid mucking about the center hexagon
            int flatRadius = (int) (radius * Math.cos(Math.toRadians(30)));
            int distance = 2 * flatRadius;
            int newX = (int) (distance * Math.cos(Math.toRadians(30 * neighborIndex)));
            int newY = (int) (distance * Math.sin(Math.toRadians(30 * neighborIndex)));
            Point p = new Point(h.getLocation().x + newX, h.getLocation().y + newY);
            neighbor.setLocation(p);
        }
        System.out.println("drawing " + neighbor + " at " + neighbor.getLocation());
        return neighbor.draw();
    }

    private static void grid(Graphics g, int height, int width) {

        Color c = Color.RED;
        for (int x = radius / 2; x < width; x += radius + radius) {
            for (int y = radius / 2; y < height; y += radius + radius) {
                if (Color.RED.equals(c)) {
                    c = Color.GREEN;
                } else {
                    c = Color.RED;
                }
                if (rotation % 60 == STANDING) {
                    c = Color.BLUE;
                }
                if (rotation % 60 == FLAT) {
                    c = Color.YELLOW;
                }
                g.setColor(c);
                g.fillPolygon(hexagon(new Point(x, y), radius, rotation));
                // todo: it's probably faster to getShape to an image and then render that, but we can look at speed later.
            }
        }
    }

    /**
     *
     * @param p the center point of the hexagon. todo: might be interesting if we started with a corner, or with the equivalent box corner.
     * @param radius the radius to the points, since it is the only one that is fixed. todo: perhaps we should look at side length instead?
     * @param rotation the rotation, in degrees, 0-360. todo: add the rotation. or perhaps simply just orientation (standing or lying)
     * @return a hexagon.
     */
    private static Polygon hexagon(Point p, int radius, int rotation) {
        int[] xs = new int[7];
        int[] ys = new int[7];

        /*
          the first point is $rotation degrees from the starting point, $radius units out.
          the next point is 60 degrees clockwise from that, $radius units out
         */

        for (int i = 0; i <= 6; i++) {
            xs[i] = (int) (p.x + Math.sin(Math.toRadians(rotation + (60 * i))) * radius);
            ys[i] = (int) (p.y + Math.cos(Math.toRadians(rotation + (60 * i))) * radius);
        }

        // close the polygon
        xs[6] = xs[0];
        ys[6] = ys[0];

        return new Polygon(xs, ys, 7);
    }
}
