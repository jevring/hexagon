package v1;

import javax.swing.*;
import java.awt.*;
import java.util.List;

/**
 * @author markus.jevring@petercam.corp
 *         Created: 2011-01-15
 */
public class ColorChanger extends Thread {
    private final List<List<Hexagon>> hexagons;
    private final float increment;
    private final JPanel canvas;
    private volatile boolean running = false;

    public ColorChanger(List<List<Hexagon>> hexagons, float increment, JPanel canvas) {
        this.hexagons = hexagons;
        this.increment = increment;
        this.canvas = canvas;
    }

    @Override
    public void run() {
        running = true;
        while (running) {
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            // todo: this can likely be moved into the paint routine,provided we can do the timeout properly, and provided it normally gets called often enough.
            // perhaps that last thing there,getting called often enough, is the big problem
            for (List<Hexagon> column : hexagons) {
                for (Hexagon hexagon : column) {
                    float hue = hexagon.getHue();
                    hue += increment * hexagon.getRate();
                    hue = hue % 1.0f;
                    Color c = Color.getHSBColor(hue, 0.5f, 0.75f);
                    hexagon.setColor(c);
                    hexagon.setHue(hue);
                }
            }
            canvas.repaint();
        }


    }

    public void shutdown() {
        running = false;
        interrupt();
    }
}
