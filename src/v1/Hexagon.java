package v1;

import java.awt.*;

/**
 * @author markus@jevring.net
 *         Date: 2011-01-13-18:52
 */
public class Hexagon {
    // todo: perhaps we should extend JComponent. That way we can light up without having to calculate
    // as long as we have a layout manager that can position them correctly, and can deal with the offset and overlap and stuff, we're fine
    public static final int ROTATION_FLAT = 30;
    public static final int ROTATION_STANDING = 0;

    private int radius;
    private int rotation = ROTATION_FLAT;
    /*
    0 = top
    n = clockwise from n-1
     */
    private final Hexagon[] neighbors = new Hexagon[6];
    private Point location;
    private Color color = Color.WHITE;
    private boolean selected = false;

    private float hue = (float) Math.random();
    private float rate = (float) Math.random() * 0.1f;

    public Hexagon(int radius) {
        this.radius = radius;
    }

    public Hexagon() {
    }

    public float getRate() {
        return rate;
    }

    public float getHue() {
        return hue;
    }

    public void setHue(float hue) {
        this.hue = hue;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
/*
        if (selected) {
            color = Main.colors[((int) (Math.random() * Main.colors.length))];
        } else {
            color = Color.WHITE; // otherwise the outline remains in the same color as when it was selected
        }
*/
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public Point getLocation() {
        return location;
    }

    public void setLocation(Point location) {
        this.location = location;
    }

    public void setNeighbor(Hexagon neighbor, int location) {
        neighbors[location] = neighbor;
        neighbor.neighbors[(location + 3) % 6] = this;
    }

    public boolean hasNeighbor(int location) {
        return neighbors[location] != null;
    }

    public Hexagon getNeighbor(int location) {
        return neighbors[location];
    }

    public Polygon draw() {
        // todo: remember the point to avoid re-generating things all the time
        return hexagon(location, radius, rotation);

        // todo: decide if they should represent the graphical element, or the logical element.
        // if it's the graphical, we should know about the location and the rotation.
        // the shape, basically.
        // if it's the logical, we only need to know about the neighbors.
    }

    /**
     *
     * @param p the center point of the hexagon. todo: might be interesting if we started with a corner, or with the equivalent box corner.
     * @param radius the radius to the points, since it is the only one that is fixed. todo: perhaps we should look at side length instead?
     * @param rotation the rotation, in degrees, 0-360. todo: add the rotation. or perhaps simply just orientation (standing or lying)
     * @return a hexagon.
     */
    private Polygon hexagon(Point p, int radius, int rotation) {
        int[] xs = new int[7];
        int[] ys = new int[7];

        /*
          the first point is $rotation degrees from the starting point, $radius units out.
          the next point is 60 degrees clockwise from that, $radius units out
         */

        for (int i = 0; i <= 6; i++) {
            xs[i] = (int) (p.x + Math.sin(Math.toRadians(rotation + (60 * i))) * radius);
            ys[i] = (int) (p.y + Math.cos(Math.toRadians(rotation + (60 * i))) * radius);
        }

        // close the polygon
        xs[6] = xs[0];
        ys[6] = ys[0];

        return new Polygon(xs, ys, 7);
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }
}
