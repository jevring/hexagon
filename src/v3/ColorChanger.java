package v3;

import javax.swing.*;
import java.awt.*;
import java.util.List;

/**
 * @author markus.jevring@petercam.corp
 *         Created: 2011-01-15
 */
public class ColorChanger extends Thread {
    private final List<List<Hexagon>> hexagons;
    private final float increment;
    private final JPanel canvas;
    private volatile boolean running = false;
    private volatile int frameRate = 20; // in fps
    private int recycleTime = 10000;
    private final GameEngine gameEngine;

    public ColorChanger(List<List<Hexagon>> hexagons, float increment, JPanel canvas, GameEngine gameEngine) {
        this.hexagons = hexagons;
        this.increment = increment;
        this.canvas = canvas;
        this.gameEngine = gameEngine;
    }

    @Override
    public void run() {
        running = true;
        while (running) {
            try {
                Thread.sleep(1000 / frameRate);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            final long now = System.currentTimeMillis();
            final boolean generalExpirationReached = now - gameEngine.getLastMatchTime() > recycleTime;
            System.out.println("Group time: " + (now - gameEngine.getLastMatchTime()));
            for (List<Hexagon> column : hexagons) {
                for (Hexagon hexagon : column) {
                    final boolean individualExpirationReached = now - hexagon.getUsedTime() > recycleTime;
                    final boolean progressiveTotalExpirationReached = (now - gameEngine.getLastMatchTime()) > recycleTime + (now - hexagon.getUsedTime());
                    System.out.println("Individual time: " + (now - hexagon.getUsedTime()));
                    System.out.println("Total time: " + ((now - hexagon.getUsedTime()) - (now - gameEngine.getLastMatchTime())));


                    /*

                    We should be able to control three things:
                    - sensitivity
                    - pulse speed
                    - timeout

                    each of these will add a multiplier to the final score, so that those that want can play on the most difficult setting

                     */

                    // todo: whenever the general timeout is reached, we start restoring them with the offset with which they were matched
                    /*
                    the used ones all have a used time
                    for them to be activated "in order", after the general timeout is reached,
                    we need to do something <-- yeah, but what?

                    we obviously can't just use the timeout of each cell, as we want the whole timeout to be reset of the user makes another match



                     */

                    if (hexagon.isUsed() && generalExpirationReached && individualExpirationReached) {
                        /*
                        That way the goal could be to clear the whole board.
                         */
                        hexagon.setUsed(false);
                    }
                    if (!hexagon.isUsed()) {
                        float hue = hexagon.getHue();
                        hue += increment * hexagon.getRate();
                        hue = hue % 1.0f;
                        Color c = Color.getHSBColor(hue, hexagon.getSaturation(), hexagon.getBrightness());
                        hexagon.setColor(c);
                        hexagon.setHue(hue);
                    }
                }
            }
            canvas.repaint();
        }
    }

    public void shutdown() {
        running = false;
        interrupt();
    }
}
