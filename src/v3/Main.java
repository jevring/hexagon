package v3;

import hexagons.engine.GameEngine;
import hexagons.graphics.Hexagon;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.util.ArrayList;
import java.util.List;

/**
 * @author markus@jevring.net
 *         Date: 2011-01-13-18:52
 */
public class Main {
    // todo: create a graphics class out of this one
    public static final int RADIUS = 50;
    private static Point mouse = new Point(0,0);
    private static List<List<Hexagon>> hexagons;
    private static GameEngine gameEngine;

    public static void main(String[] args) {
        final JFrame frame = new JFrame("Hexagon");
        final JPanel canvas = new JPanel() {
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g); // paints the background
                drawHexagons(g);
            }
        };
        canvas.setBackground(Color.BLACK);
        canvas.addMouseMotionListener(new MouseMotionAdapter() {
            public void mouseMoved(MouseEvent e) {
                mouse = e.getPoint();
                canvas.repaint();
            }
        });
        // control the size of the board, not the size of the window.
        final Dimension size = new Dimension(800, 480);
        canvas.setMinimumSize(size);
        canvas.setPreferredSize(size);
        frame.add(canvas);
        frame.pack();
        frame.setLocation(600, 600);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        createHexagons(size.height + RADIUS / 2, size.width + RADIUS / 2);
        gameEngine = new GameEngine(hexagons, canvas, 0.15f);
        canvas.addMouseListener(gameEngine);
        frame.setVisible(true);
        gameEngine.start();
    }


    private static void drawHexagons(Graphics g) {
        Graphics2D g2d = (Graphics2D)g;
        g2d.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_DEFAULT);
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON); // WORLD OF DIFFERENCE!
        int x = 0;
        for (List<Hexagon> column : hexagons) {
            int y = 0;
            for (Hexagon hexagon : column) {
                Polygon polygon = hexagon.getShape();
                g.setColor(hexagon.getColor());
                g.fillPolygon(polygon);
                g.setColor(Color.LIGHT_GRAY);
                // the game tiles should probably always have some form of border. It's just that the border changes when it is highlighted

                if (polygon.contains(mouse) || hexagon.isSelected()) { // todo: obviously when we're doing this for android, we can't use the mouse-over
                    g.setColor(Color.WHITE);
                    g2d.setStroke(new BasicStroke(5f));
                } else {
                    g2d.setStroke(new BasicStroke(2f));
                }
                g.drawPolygon(polygon);

                g.setColor(Color.WHITE);
                //g.drawString(x + "," + y, hexagon.getLocation().x, hexagon.getLocation().y);
                y++;
            }
            x++;
        }
    }

    private static void createHexagons(int height, int width) {
        /*
        drawing hexagons in a grid is easy. Each one is just one full distance down
        or half a distance down and half a distance out
         */
        hexagons = new ArrayList<List<Hexagon>>(); // recreate the set when we resize the panel

        boolean offset = true;
        int border = 10;
        int flatRadius = (int) (RADIUS * Math.cos(Math.toRadians(30)));
        int offsetDistance = (int) (RADIUS * Math.sin(Math.toRadians(30))) + RADIUS;
        for (int x = RADIUS; x < width; x += offsetDistance + border) {
            // todo: change this so that there is a "frame" or partial hexagons framing the entire game board.
            // this frame should also pulsate, but should not be selectable.
            // only fully visible hexagons should be selectable.


            // _todo: every other line needs to be offset by radius
            List<Hexagon> column = new ArrayList<Hexagon>();
            hexagons.add(column);
            //for (int y = (flatRadius * (offset ? 1 : 2)) + (offset ? 0 : border / 2); y < height; y += (flatRadius * 2) + border) {
            int startingPoint;
            if (offset) {
                // ((flatRadius * (offset ? 1 : 2)) + (offset ? 0 : border / 2))
                startingPoint = flatRadius;
            } else {
                startingPoint = -(flatRadius / 2) + border + (border / 2) ;
            }
            for (int y = startingPoint; y < height; y += (flatRadius * 2) + border) {
                Hexagon h = new Hexagon(RADIUS);
                h.setLocation(new Point(x, y));
                column.add(h);
            }
            offset = !offset;
        }
    }
}
