package v3;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

/**
 * @author markus.jevring@petercam.corp
 *         Created: 2011-01-16
 */
public class GameEngine extends MouseAdapter {
    private final List<List<Hexagon>> hexagons;
    private final JPanel canvas;
    private final ColorChanger colorChanger;
    private float difficulty = 0.3f;
    private int score = 0;
    private long lastMatchTime = 0;
    private int matches = 0;

    public GameEngine(List<List<Hexagon>> hexagons, JPanel canvas, float speed) {
        this.hexagons = hexagons;
        this.canvas = canvas;
        this.colorChanger = new ColorChanger(hexagons, speed, canvas, this);
    }

    public void start() {
        colorChanger.start();
    }

    public void stop() {
        colorChanger.shutdown(); // can't do "stop()", because that's a final method in Thread
    }

    @Override
    public void mousePressed(MouseEvent e) {
        Hexagon otherSelectedShape = null;

        for (int x = 0; x < hexagons.size(); x++) {
            List<Hexagon> column = hexagons.get(x);
            for (int y = 0; y < column.size(); y++) {
                Hexagon hexagon = column.get(y);
                if (hexagon.isUsed()) continue; // only play with unused tiles
                Polygon polygon = hexagon.getShape();
                if (polygon.contains(e.getPoint())) {
                    hexagon.setSelected(!hexagon.isSelected());
                }
                if (hexagon.isSelected()) {
                    lastMatchTime = Math.max(System.currentTimeMillis(), lastMatchTime);
                    if (otherSelectedShape == null) {
                        otherSelectedShape = hexagon;
                    } else {
                        if (Math.abs(otherSelectedShape.getHue() - hexagon.getHue()) <= difficulty) {
                            hexagon.setColor(Color.WHITE);
                            otherSelectedShape.setColor(Color.WHITE);
                            //hexagon.setHue((float) Math.random());
                            //otherSelectedShape.setHue((float) Math.random());
                            System.out.println(++score);
                        } else {
                            // todo: instead of doing this, set two random previously used colors back to unused
                            hexagon.setColor(Color.BLACK);
                            otherSelectedShape.setColor(Color.BLACK);
                            System.out.println(--score);

                            // todo: also, after a while, even the "correct" tiles can start changing color again
                            // todo: when we do, we have to reduce the score

                        }
                        hexagon.setSelected(false);
                        otherSelectedShape.setSelected(false);
                        hexagon.setUsed(true);
                        otherSelectedShape.setUsed(true);
                    }
                }
            }
        }
        if (score >= Math.floor(45 / 2)) { // 45 tiles, but we require 2 for a match, so
            System.out.println("We've got a winnah!");
        }
        canvas.repaint();
    }

    public long getLastMatchTime() {
        return lastMatchTime;
    }

    public int getMatches() {
        return matches;
    }

    public int getScore() {
        return score;
    }
}
